console.log('Hello, World')
// Details
const details = {
    fName: "Potato",
    lName: "Man",
    age: 69,
    hobbies : [
        "playing", "biking", "drawing"
    ] ,
    workAddress: {
        housenumber: "Lot 6, Block A",
        street: "Zuitt Street",
        city: "Metronome City",
        state: "Free State",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");